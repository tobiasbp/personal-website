---
author: admin
date: 2006-07-07 03:18:39+00:00
draft: false
title: 4.7.2006 - Sapporo - Otaru
type: post
url: /2006/07/sapporo-otaru/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Vi vågnede først kl. 10 morgenen og måtte hurtigt ud da man skal være ude før 10. Der var heldigvis ingen problemer, og damen på hotellet hjalp os enda med at lave vand til vores medbragte instant-nudles. Sapporo var der fuldt af udlændinge som vi også have bemærket aftenen før. Det viste sig at det var fordi der lå et amerikansk hangarskib i en havn tæt på byen. Amerikanerne fyldte meget i billedet. Vi tog toget til en by på kysten der hedder Otaru. Det var en meget flot rute der gik langs kysten. Vendt på den ene side og bjerge på den anden. De var meget stolte af nogle bygninger fra slutningen af 1800 tallet, men vi synes det var sjovere at se nogle gamle bank-bygninger fra 1920'erne. Fra stationen tog vi en bus ud for at se nogle huse der har forbindelse til det tidligere meget indbringende silde-fiskeri. Først så vi en meget luksuriøs villa i japansk stil, dog med et meget autentisk værelse i vestlig stil. Komplet med stuk og hvad der nu ellers skal være i sådan et herskabeligt værelse. Huset blev bygget af en familie der var blevet meget rige af silde-fiskeri. I husets have vorksede der et [Ume](http://en.wikipedia.org/wiki/Ume)-træ. Vi samlede nogle nedfaldne frugter så vi kan prøve at plante dem i Danmark. Der bliver meget koldt i Hokkaido om vinteren, så hvis de kan dyrke ume der, må det også kunne lade sig gøre i Danmark. Vi gik lidt længere langs kysten og besøgte et stort gammelt hus hvor både en ejer af en fiske-flåde og hans arbejdere havde boet. Det var sjovt og se og vi fik taget et billede af os i autentisk arbejdstøj. Huset lå oppe på en klippe og man kunne se ned vandet der så krystalklart ud. Bagefter spiste vi en grillet sild hver. Jeg synes det er så svært at spise fisk med alle de ben, men Rie havde som altid ikke nogen problemer med det. Da vi kom tilbage til Sapporo fandt vi ved et tilfælde en Book-Off (Sælger brugte bøger, spil og CD'er). Sådan en havde jeg gået og ledt efter, så vi måtte straks ind og kigge. Jeg brugte selvfølgelig mest tid på at kigge på spil. Rie købte nogle bøger og jeg fandt et par [Doraemon](http://en.wikipedia.org/wiki/Doraemon)-bøger jeg ikke havde. Efter bogindkøb læste Rie på en bænk og jeg gik rundt for at finde et åbent trådløst netværk så jeg kunne komme på nettet. Jeg fik uploadet billeder og de rejsebeskrivelser jeg havde forberedt offline. Vi overnattede igen på vores yndlings love hotel. Denne gang i et nyt værelse med nedslidt guld-loft. En anden feature som også det forrige værelse havde, var et stort vindue mellem sengen og  badet så man kan beundre sin afklædte partner vaske sig.

[bubbleshare 44415.f418c7ce917]
