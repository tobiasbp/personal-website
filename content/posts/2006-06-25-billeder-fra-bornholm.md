---
author: admin
date: 2006-06-25 22:13:20+00:00
draft: false
title: Billeder fra Bornholm
type: post
url: /2006/06/billeder-fra-bornholm/
categories:
- Billeder
- Rejse
---

Rie og jeg var på Bornholm sidste år for at fejre Steffen og Hanaes bryllup. Vi boede sammen med Martin og Aya i et lejet sommerhus. Her er billeder fra vores tur på øen.
[bubbleshare 42089.65ebdedea05]
