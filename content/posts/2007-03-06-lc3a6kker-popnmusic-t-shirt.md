---
author: admin
date: 2007-03-06 21:58:08+00:00
draft: false
title: Lækker Pop'n'music T-Shirt
type: post
url: /2007/03/lækker-popnmusic-t-shirt/
categories:
- Gaming
- Japan
---

Så en [Pop'n'music](http://en.wikipedia.org/wiki/Pop%27N_Music) T-Shirt udgivet i anledning af seriens 10 års jubilæum. En sjov detalje er, at der på forsiden bl.a. står "Knall musik". Det må næsten være fordi musikken en knald god eller hvad? <del>Du kan se [større billeder ](http://www.graniph.com/product/detail.php?designID=001002350) der hvor de sælger trøjen</del>.
