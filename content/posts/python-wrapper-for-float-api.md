---
title: "Python wrapper for Float Api"
date: 2020-06-06T17:13:01+02:00
draft: false
---
Lately I have been working on a python wrapper for the API of the project and
schedule management service at [www.float.com](https://www.float.com).

The wrapper supports most of the Float API at this point. The source code  as well
as a longer description is [available on github](https://github.com/tobiasbp/float_api).

There is [a package available on PyPI](https://pypi.org/project/float-api/) which allows for easy
install with the Python package manager _pip_ using the following command: _pip install float-api_

To access the API, you need to get an access token from your Float account settings page.

Here is an example of getting all the _people_-objects from a Float account.

```
# Import the API wrapper
from float_api import FloatAPI

# Instantiate API object
api = FloatAPI(YOUR_SECRET_ACCESS_TOKEN)

# Get a list of all people
people = api.get_all_people()

# Print all the people entries
for p in people:
  print(p)

```
