---
author: admin
date: 2006-07-07 03:20:00+00:00
draft: false
title: 6.7.2006 - Sapporo - Touya - Aomori
type: post
url: /2006/07/sapporo-touya-aomori/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Forlod hotellet i Sapporo klokken 10 hvor det det regnede.  Vi blev hurtigt enige om at vi ikke gad være i Sapporo mere, så vi tog toget sydpå til Touya hvor man kan se vulkaner. Den største, Usuzan,  senest var  i udbrud i år 2000. Efter en togtur på ca. 90 minutter ankom vi til Touya. Vi låste vores rygsække ind på stationen og tog bussen op til området omkring Usuzans krater som, udover anlagte stier, er blevet efterladt som det så ud efter det seneste udbrud. Det regnede stadig meget, men vi har efterhånden indset at vi bare må ignorere det. Det var spændende at se dampene sive op af jorden og mærke at røre ved de varme klipper. Der var nogle ødelagte mindre bygninger i området som var blevet efterladt son en del af turistatraktionen. Alt i alt sjovt for en midaldrene rask dreng som mig. Da vi kom ned fra krateret kom der en bus som vi lige nåede ved at løbe.  Vi kørte til Touya by som ligger ved en meget stor sø oppe bjergene. Der regnede og var meget regnfuldt, så vi besøgte et museum om områdets vulkaner. Efter museet spiste vi ramen i håbet om at regnen ville oophøre før vi skulle ud og se et boligkompleks og et badeanstalt som blev ødelagt af muddermasserne i forbindelse med udbruddet i år 2000. Det var sjovt og lidt uhyggeligt at se de ødelagt bygninger, og Rie ville ikke være med på nogle billeder af dem. Japanere er bange for spøgelser, og dem kunne man jo godt forestille sig at der var nogle af i de ødelagte huse selvom der ikke døde nogle mennesker i forbindelse med udbruddet. Touya der er en by der lever af turisme omkring de varme kilder og den smukke sø, har en masse  hoteller med tilknyttede bade man kan besøge uden at være overnattende gæst. Vi badede, hver for sig, i en time og tog bussen tilbage til Touya-station. Vi har haft meget glæde af en tog-køreplan vi har fået af Ries far, og vi skulle kun vente 10 minutter på toget. Vi kørte tilbage til Aomori der ligger på Honshuu. En tur på ca. 4 timer. Kl. ca. 22:30 checkede vi ind på samme love hotel som vi tidligere har overnattet. På hotellet viste det sig at man kunne man få en gratis øl leveret til værelset. Da det ringede på, gik jeg ud og åbnede, men damen der kom med øllen gemte sig bag døren og rakte kun bakken frem. At det hele skal foregå så diskret synes jeg er ret morsomt. Da vi gik i seng regnede det stadig.

[bubbleshare 45120.349e7bd553d]
