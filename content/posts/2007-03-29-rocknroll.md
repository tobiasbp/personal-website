---
author: admin
date: 2007-03-29 22:03:30+00:00
draft: false
title: Rock'n'Roll
type: post
url: /2007/03/rocknroll/
categories:
- Gaming
- Japan
- Life
- Musik
---

Der er sket noget meget underligt. Jeg er begyndt at høre rockmusik. Jeg tror det er fordi jeg har spillet meget [GuitarFreaks](http://en.wikipedia.org/wiki/Guitarfreaks) og [DrumMania](http://en.wikipedia.org/wiki/DrumMania) på det seneste. Det er ligesom med [Dance Dance Revolution](http://en.wikipedia.org/wiki/Dance_dance_revolution). Da jeg begyndte at spille det, blev jeg ret hurtigt vild med [EuroBeat](http://en.wikipedia.org/wiki/Eurobeat) og det er jeg stadig. Nu er turen altså kommet til japansk rock. Der er et virkeligt godt band der hedder [The Pillows](http://en.wikipedia.org/wiki/The_pillows). De har en music-player på [deres myspace](http://www.myspace.com/thepillowsfromjapan) hvor man kan høre en masse numre. Jeg er ikke stødt på et dårligt nummer endnu. Det er helt vild imponerende synes jeg. Der er blandt andet et nummer der hedder noget så lækkert som "Rock'n'Roll sinners" der virkelig rykker. Hør det, hør det!
