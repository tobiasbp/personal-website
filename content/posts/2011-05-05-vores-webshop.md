---
author: admin
date: 2011-05-05 07:54:02+00:00
draft: false
title: Vores webshop
type: post
url: /2011/05/vores-webshop/
categories:
- Life
---

NÃ¥Ã¥Ã¥hhh ekstremt stillestÃ¥ende blog...... Det sker ikke sÃ¥ meget... Faktisk er der jo snart ikke sket noget her i hele tre Ã¥r! Men lidt er der faktisk sket i live trods alt ;). Rie og jeg er begyndt at sÃ¦lge skandinaviske brugsgenstande fra perioden fra 1950 til 1970 pÃ¥ nettet. I fÃ¸rste omgang henvender vi os kun til det japanske marked. Vi sÃ¦lger [åæ¬§ã´ã£ã³ãã¼ã¸éè²¨](http://www.sonotoki.jp) (Scandinavian vintage goods). Det er mÃ¦rker som svenske [RÃ¶rstrand/ã­ã¼ã«ã¹ãã©ã³ã](http://www.sonotoki.jp/rorstrand) og norske [Stavangerflint/ã¹ã¿ã´ã¡ã³ã²ã«ããªã³ã](http://www.sonotoki.jp/stavangerflint). Vi har ogsÃ¥ platicdÃ¥ser fra [Erik Kold/ã¨ãªãã¯ã»ã³ã«ã](http://www.sonotoki.jp/erik_kold) med retro-blomster. Dem er der mange der godt kan lide. Vi forsÃ¸ger os ogsÃ¥ med [åæ¬§ã?ã¹ã¿ã¼](http://www.sonotoki.jp/hokuou_scandinavian_posters)/skandinaviske plakater. Vi har bÃ¥de originaler og genoptryk.
