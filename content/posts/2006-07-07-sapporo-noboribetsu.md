---
author: admin
date: 2006-07-07 03:19:27+00:00
draft: false
title: 5.7.2006 - Sapporo - Noboribetsu
type: post
url: /2006/07/sapporo-noboribetsu/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Vi forlod hotellet før kl. 10 i dag. Vi gik til fiskemarkedet hvor vi spiste sushi. Bagefter var vi oppe på 19. etage i rådhuset hvofra man kan se ud over byen. Jeg bryder mig ikke om højder, så jeg fik ret hurtigt nok. Bagefter så vi et hus fra slutningen af 1800 tallet som skulle være meget kendt. Huset blev restaureret for nogle år siden, og det var sjovt at se film om det arbejde. Da vi var ved at være lidt trætte af Sapporo besluttede vi at køre lidt sydpå igen. Da vi havde kørt omkring 1 time var det igen fuldstændig overskyet, og vi valgte at stå af i byen Noboribetsu der er kendt for sine onsen (varne kilder). Ved stationen ligger der en mindre forlystelsespark med et slot der ligner en kopi af Egeskov. Rie mener at have læst om en kopi af Egeskov i Hokkaido. Vi må undersøge sagen på nettet. Sammen med 40 pensionister steg vi på bussen og kørte mod Noboribetsu-onsen der ligger oppe i bjergene. Det varme vand kommer op af jorden i en dal der hedder Jikoku-dani (Helvedes dalen) pga. svovldampene. Vi var oppe og kigge, og det så ret imponerende ud. Efter at have set kildens udspring gik vi ind på en onsen og badede indtil vi skulle hjem med bussen. Det var dejligt at bade, men også lidt kedeligt da man ikke kan bade sammen. Vi tog toget tilbage til Sapporo hvor der stadig var skyfrit. Ved stationen kiggede jeg efter gratis trådløst netværk. Det var et godt et ved stationen, så vi blev der i et stykke tid for at uploade billeder til websitet og læse mail og nyheder. Bagefter fandt vi fremt til en kendt lille gyde der kun har ramen-restauranter. Jeg fik en vældig god kimchi-ramen. Vi sover igen på vores ny vante hotel. Igen i nat under guld-loft.

[bubbleshare 45108.71613f0beed]
