---
author: admin
date: 2008-08-20 21:38:49+00:00
draft: false
title: Billeder fra sommeren 2008 i Alsted
type: post
url: /2008/08/billeder-fra-sommeren-2008-i-alsted/
categories:
- Billeder
- Life
---

Jeg har lagt billeder fra denne sommer i Alsted op [her](http://picasaweb.google.com/tobiasbp/AlstedSommer2008). Se hvor stor Marie er blevet. <del>Nedenfor kan du se billederne direkte her på siden.</del>
