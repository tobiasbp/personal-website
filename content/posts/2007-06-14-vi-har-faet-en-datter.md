---
author: admin
date: 2007-06-14 20:54:33+00:00
draft: false
title: Vi har fået en datter.
type: post
url: /2007/06/vi-har-faet-en-datter/
categories:
- Life
---

I søndags kl. 18:59 fødte Rie vores datter. Hun skal hedde Marie og ligner fulstændig Rie som hun så ud som spæd. Det gik helt utroligt godt.
Da fødslen først var kommet i gang var det hele overstået på 9 minutter. Rie var RIGTIG god!

![Marie i vugge](/wpexport/2007-06-14-vi-har-faet-en-datter/img_3842-2.JPG)

