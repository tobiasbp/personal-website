---
author: admin
date: 2006-07-25 08:14:45+00:00
draft: false
title: 21.7.2007  Tokyo - Gunma - Niigata
type: post
url: /2006/07/2172007-tokyo-gunma-niigata/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Den næst-sidste dag på vores rail pass. Da vi forlod hotellet klokken 10:30 regnede det. Vi har aftalt at besøge Ries mors søster i aften i [Niigata](http://en.wikipedia.org/wiki/Niigata_City), så vi skulle rejse mod nord. Kiggede lidt i guidebogen og fandt et område med en massen [onsen](http://en.wikipedia.org/wiki/Onsen) der hedder [Gunma](http://en.wikipedia.org/wiki/Gunma). Området ligger nord for Tokyo på vej mod Niigata. Tog toget til [Ueno station](http://en.wikipedia.org/wiki/Ueno_Station), hvor vi skiftede til [Shinkasen](http://en.wikipedia.org/wiki/Shinkansen) mod Niigata. Stod af efter en time, og skiftede til lokalt tog for at komme til en onsen-by i bjergene. Tog en bus det sidste stykke op til byen. Der var en anden udlænding med bussen der viste sig at være fra Tyskland og hedde Jan. Han er japansk gift, og var også på japan-tur med sin kone og deres tre børn. Vi gik sammen ud for at finde et godt sted. Endte i en lille offentlig udendørs onsen. Meget dejligt sted. Det var meget hyggeligt at tale med Jan om en udlændings syn på og oplevelser i og med det japanske. Bagefter gik vi til en anden onsen i byen. Der havde de mange flere bade etc., men den var mindre autentisk. Jeg tror bedst jeg kunne lide den første. Tog bussen tilbage til stationen og spiste nudler på en cafe. Fulgtes med Jan i toget. Han skulle af tidligere end os, da han skulle hjem til familien som var indkvarteret hos svigermoderen. Vi tog shinkansen til Niigate som vi ankom til klokken 19:30. Tog et lokalt tog til Kobari i Niigata. Rie ringede til sin moster som gik os i møde. Hjemme fik vi serveret en enorm aftensmad. Mosteren havde været i skandinavien, og kunne huske at man fik enormt meget mad i Danmark, så hun havde sørget for at der var nok at spise til mig. Det var vældig hyggeligt, og maden var rigtig god. Efter maden kiggede jeg med interesse på en [Famicom](http://en.wikipedia.org/wiki/Famicom) der stod i stuen. Mosteren fortalte at hendes yndligs-spil spil var [Dr. Mario](http://en.wikipedia.org/wiki/Dr._Mario), og at hun altid spillede det på højeste sværhedsgrad. Derefter spurgte hun om jeg ville have alle de andre spil hun ikke brugte, og det ville jeg selvfølgelig gerne. Jeg fik så både nogle Famicom og [Super Famicom](http://en.wikipedia.org/wiki/Super_Famicom) spil. Bagefter foreslog tanten at vi skulle synge [karaoke](http://en.wikipedia.org/wiki/Karaoke), og det ville jeg selvfølgelig gerne. I det værelse hvor Rie og jeg skulle sove satte hun så sin karaoke-maskine til. Mosteren var dog kun med lige i starten, derefter tog Rie og jeg over og hun kom op med drikkevarer. En rigtig hyggelig aften.

[bubbleshare 51392.6c04b0db003]
