---
author: admin
date: 2006-07-04 11:19:24+00:00
draft: false
title: 3.7.2006 - Hakodate - Sapporo
type: post
url: /2006/07/hakodate-sapporo/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Det regnede stadig da vi stod op om morgenen i Aomori. Vi måtte aflyse at tage ud og se den store buddha. I stedet tog vi toget til Hakodate [GP:hakodate] som ligger på Hokkaidos sydlige kyst. Folk siger at der ikke er regntid på Hokkaido, så vi glædede os til sol fra en skyfri himmel. Efter at have kørt gennem verdens længste undervands-tunnel var vi på Hokkaido. Det var til vores store skuffelse stadig totalt overskyet, men det regnede dog ikke. Hakodate var sammen med Yokohama en af tre byer der førts blev brugt til at handle med vesten. Derfor har Hakodate en del bygninger fra den tid. Vi var rundt og se nogle eksempler på huse opført i vestlig stil. Der var også et museum Hokkaidos oprindelige befolkning Ainu. Vi fik spillet på en Ainu-mundharpe. På fiskemarkedet så vi nogle gigantiske krabber på over 2 kg. Det var også muligt at få serveret en frisk-slagtet blæksprutte man kunne vælge fra et stort kar. De små arme vred sig stadig på tallerkenen når der blev serveret. Vi smagte dog ikke på sagerne og nøjedes med at kigge. Lidt over klokken 4 tog vi toget mod Sapporo [GP:sapporo] som er Hokkaidos hovedstad og japans 5. største by. Vi ankom kl. 7 og det var fulstændig som at være i Tokyo, men med lidt færre mennesker. På den anden side virker Sapporo faktisk større fordi hele byens forretningsliv er samlet et sted i modsætning til Tokyo hvor hver station opleves som en mere eller mindre selvstændig by. Vi spiste Sapporo-Ramen , som dog smagte meget traditionelt, før vi gik på jagt efter dagens Love Hotel. På kortet i vores guide var der markeret et "Love hotel district" så der var ikke så meget tvivl om hvor vi skulle gå hen. Vi nåede til et område med næsten udelukkende sex-relateret forretningsliv. Det viste sig at være et meget kendt område, og Rie fortalte om hvordan de altid viste at cheferne på hendes gamle arbejde skulle derhen når de var på foretningsrejse til Sapporo. Vi fandt hurtigt hotellerne og gik or kiggede på priser. Vi kunne ikke sige nej til at sted vor en overnatning kunne fås til den rystende billige pris af 165 danske kroner. Hotellet var selvfølgelig nedslidt og var nok bygget sidst i 60'erne eller først i 70'erne, men det er da utroligt at man kan bo i midten af en storby til den pris. 

[bubbleshare 44142.3508ebbbc43]
