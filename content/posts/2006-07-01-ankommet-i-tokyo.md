---
author: admin
date: 2006-07-01 14:47:38+00:00
draft: false
title: Ankommet i Tokyo
type: post
url: /2006/07/ankommet-i-tokyo/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Saa er vi i Tokyo. Vi kom i går. Turen gik rigtig fint, og vi fik begge sovet på flyet. I dag har vi været ude at gå i området omkring Ries fars hus ([GP:tama]). Rie har købt bøger, og jeg har kigget på spil og andre spændene sager. Det er nu den tid på året hvor raske drenge samler store biller i naturen og lader [dem](http://bricolajeyocio.com/391/) kæmpe mod hinanden. Vi var inde og kigge i en legetøjsbutik hvor de havde helt utroligt meget bille-udstyr. Jeg har aldrig set denne hobby fylde så meget før. Man kunne også købe biller i butikken, og for dem der har finanserne i orden havde de nogle kæmpe biller der kostede over DKR 2000,- stykket.




Ingen af os har tænkt over at det er regntid nu. Det er overskyet og regner over hele landet undtagen Hokkaido hvor vi så tager op i morgen med lyntog (Shinkansen). Det bliver en lang tur. Omkring 10 timer i alt. Vi håber at det bliver bedre vejr sydpå medens vi udforsker de nordlige provinser.




[bubbleshare 44151.16e9809cfb6]



