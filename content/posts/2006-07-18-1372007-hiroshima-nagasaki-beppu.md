---
author: admin
date: 2006-07-18 12:00:36+00:00
draft: false
title: 13.7.2007 Hiroshima - Nagasaki - Beppu
type: post
url: /2006/07/1372007-hiroshima-nagasaki-beppu/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Tog toget til [Nagasaki](http://en.wikipedia.org/wiki/Nagasaki). Først med Shinkansen i 75 minutter og så med almindeligt tog i 2 timer. Man bliver forvent, og synes pludselig at de normale tog, selvom de er express, kører utroligt langsomt. Da vi kom ud af toget i Nagasaki var det helt utroligt varmt. Vi tog en sporvogn til byens atombombe-museum. Det var spændende, men meget af historien er jo den samme som vi allerede havde lært om i Hiroshima. På museet i Nagasaki var der dog lagt større vægt på atomvåbens udvikling og historie. Efter museet besøgte vi freds-parken og andre steder med tilknytning til atombomben. Efter at have studeret guide-bogen, blev vi enige om at der ikke var mere vi ville se i Nagasaki. Vi tog toget til [Beppu](http://en.wikipedia.org/wiki/Beppu) som ligger på østsiden af [Kyuushuu](http://en.wikipedia.org/wiki/Kyushu). I vores bog stod der at byen er kendt for varme kilder og sexindustri, og det lød jo spændende. Vi ankom til Beppu klokken 23:30 og gik til et youth hostel som vi tidligere havde ringet til. Der var kakerallakker i badeværelset og vores værelse var meget primitivt. En stor omvæltning fra vores love hotel i Hiroshima. Det var tilmed det dyreste sted vi indtil videre har boet på. Vi bestilte morgenmad som skulle indtages mellem klokken 7 og 8.

[bubbleshare 49624.5618e9f2ba7]
