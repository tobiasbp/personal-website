---
author: admin
date: 2006-08-07 21:42:32+00:00
draft: false
title: Onyanko Club
type: post
url: /2006/08/onyanko-club/
categories:
- Japan
---

Rie har introduceret mig for en fantastisk 80'er jpop sang der hedder "Se-ra- fuku wo nugasanaide". Den handler om en ung pige der vakler mellem at have sex og at lave sine lektier. Sangen er originalt med [Onyanko Club](http://en.wikipedia.org/wiki/Onyanko_club), men her i en ny indspilning af [Morning Musume](http://en.wikipedia.org/wiki/Morning_Musume). Der er en spændende side med mere Onyanko Club info og sangens tekst på engelsk [her](http://www.pliink.com/mt/marxy/archives/2005_03.html#000422).

Update 4-3-2007:
Videoen er ikke længere tilgængelig på youTube
