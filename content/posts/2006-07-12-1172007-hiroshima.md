---
author: admin
date: 2006-07-12 16:17:35+00:00
draft: false
title: 11.7.2007 - Hiroshima
type: post
url: /2006/07/1172007-hiroshima/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Om morgenen besøgte vi [Hiroshima slot](http://en.wikipedia.org/wiki/Hiroshima_Castle). Der var ikke så stort som det i Osaka, men her kunne vi da komme ind. Det var sjovt at gå rundt i slottet. Der var meget fine engelske tekster på alle plancher. De videoer der var, havde også mulighed for at høre tale på engelsk. Udover historiske effekter var der også en udstilling af børnetegninger der var blevet lavet med parken og slottet som tema. Der var mange virkelig flotte og sjove. På øverste etage kunne man gå udenfor og se ud over byen. Efter en time på slottet tog vi op til stationen for at tage et tog ud til en fortstad hvor Mazda har en fabrik. Vi var med på en [rundvisning](http://www.mazda.com/museum/) på engelsk der startede kl. 13. Der var nogle rigtige bil-nørder med på turen, men guiden vidste tilysenladende intet om biler hvilket var ret pinligt synes jeg. Alt i alt en ret kedelig tur, selvom det faktisk var ret sjovt at se de enorme transportbånd i en af hallerne. Fabrikken fylder et kæmpe område, og indeholder bl.a. verdens længste privatejede bro. Tilbage i Hiroshima besøgte vi et manga-bibliotek der ligger inde i en park. Biblioteket var meget vel besøgt, og jeg fik læst en enkelt historie i en letlæselig manga. I parken lå også et tempel som mange havde flygtet til efter atombomben. Vi tog en sporvogn væk fra parken. Der er mange forskellige typer af sporvogne i brug i byen. Den vi kørte med virkede meget gammel. Det var nærmest som en museumstur. Vi spiste is ved floden og jeg fik skrevet lidt til websitet. Til aftensmad spiste vi igen okonomiyaki. Denne gang bestilte vi to forskellige varianter og delte. Bagefter var vi nede i freds-parken og se monumenterne ved nattetide. Vi sad på en bænk og snakkede og jeg fik mig en øl. Klokken ti checkede vi ind på samme hotel som dagen før. Da vi tændte fjernsynet var der et underholdningsprogram hvor seks personer hver for sig tog en tur i en voldsom rutschebane, hvorefter panelet i studiet, på baggrund af personernes opførsel under turen, skulle sige hvem af de seks der var transvestitter. Meget underholdende og japansk.

[bubbleshare 46580.7c7e96189ea]
