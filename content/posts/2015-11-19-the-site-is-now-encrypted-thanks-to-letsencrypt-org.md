---
author: admin
date: 2015-11-19 10:59:10+00:00
draft: false
title: The site is now encrypted thanks to letsencrypt.org
type: post
url: /2015/11/the-site-is-now-encrypted-thanks-to-letsencrypt-org/
categories:
- Tutorial
---

[Let's Encrypt](https://letsencrypt.org/), offers free certificates. I'm in the beta program, so I have been able to get certificates for this site early on. It's really easy. There is a piece of software, which gets the certificates and configures you webserver. For now Apache and Nginx are supported. [Read the documentation](https://letsencrypt.readthedocs.org/en/latest/using.html).

I'm using [lighttpd](http://www.lighttpd.net/), so I need to do a bit of manual work.  Lighttpd needs to be configured with two certificate-related files pr. domain.

<!-- more -->

**ssl.ca-file** is the file **/etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem**

**ssl.pemfile** is the the following files combined (Use the command **cat**:
**/etc/letsencrypt/live/DOMAIN_NAME/cert.pem**
**/etc/letsencrypt/live/DOMAIN_NAME/privkey.pem**

I have two domains on my server: opfinderklubben.dk and balle-petersen.org.

`
$SERVER["socket"] == "0.0.0.0:443" {
ssl.engine = "enable"
ssl.use-sslv2 = "disable"
ssl.use-sslv3 = "disable"
ssl.cipher-list = "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE$
ssl.dh-file = "/etc/ssl/private/dhparams.pem"
ssl.honor-cipher-order = "enable"
ssl.ec-curve = "secp384r1"
ssl.pemfile = "/etc/lighttpd/opfinderklubben.dk.certkey.pem"
ssl.ca-file = "/etc/lighttpd/opfinderklubben.dk.fullchain.pem"
$HTTP["host"] =~ "(^|www\.)balle-petersen.org" {
ssl.pemfile = "/etc/lighttpd/balle-petersen.org.certkey.pem"
ssl.ca-file = "/etc/lighttpd/balle-petersen.org.fullchain.pem"
}
}
`
The certificates expire after 90 days, so the should be renewed (using the tool) after, say, 60 days.

Read the [tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-multiple-ssl-certificates-on-one-ip-with-lighttpd) form Digtial Ocean on how to secure more than one site using Lighttpd and a single IP.
