---
author: admin
date: 2006-07-18 11:56:39+00:00
draft: false
title: 12.7.2007 Hiroshima - Mijajima - Hiroshima
type: post
url: /2006/07/1272007-hiroshima-mijajima-hiroshima/
categories:
- Billeder
- Rejse
- Rejse til Japan 2006
---

Tog sporvognen i en time ned til havnen. Tog en lille færge til en ø der hedder Mijajima. Her ligger et templet [Itsukushima](http://en.wikipedia.org/wiki/Itsukushima_Shrine) med den kendte røde [port](http://en.wikipedia.org/wiki/Torii_Gate) som er en af de mest kendte seværdigheder i Japan. Da vi gik fra borde blev vi mødt af en masse hjorte der gik frit rundt ligesom i Nara. På trods af lidt regn da vi forlod hotellet, var vejret nu rigtig dejligt vejr. Vi købte is og gik rundt i byen og kiggede på de forskellige seværdigheder. Det var lavvande, så  templet,der er bygget så det "flyder" på vandet ligesom porten, stod på den blottede havbund. Man kunne gå helt ud til porten. Der var fyldt af ældre mennesker der gravede efter muslinger i havbunden. Herefter gik vi op ad bjerget på øen. De tog omkring en time at gå hele vejen. Det var en fantastisk udsigt fra toppen af bjerget . Efter at have fået lidt at drikke, gik vi ned igen. På vejen så vi noget jordskredssikring som var lavet i japansk-have stil. Det var virkelig flot. Vi spiste [zarusoba](http://en.wikipedia.org/wiki/Soba) på en restaurant og sad lidt og nød udsigten over vandet. Pludselig kunne vi se at vandet var begyndt at stige, og vi besluttede at vente på tidevandet så vi kunne se porten i "flydende" tilstand. Vi var ikke de eneste der ventede på vandet. Der var fyldt at ældre japanske mænd med avanceret fotoudstyr. Klokken blev 21 før vi forlod øen. Vi sov på det fantastiske [Celeb Plaza Hotel](http://www.cph.in) i Hiroshima igen. Denne gang i et lidt dyrere værelse til 6000 yen med massagestol og tv-projektor.

[bubbleshare 48853.94bb3a84d33]
