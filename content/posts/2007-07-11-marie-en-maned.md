---
author: admin
date: 2007-07-11 19:00:51+00:00
draft: false
title: Marie en måned
type: post
url: /2007/07/marie-en-maned/
categories:
- Billeder
- Life
---

Så er Marie blevet en måned gammel.

Her er flere billeder. Klik på dem for at se større versioner.

![Marie Juli 2007 1](/wpexport/2007-07-11-marie-en-maned/img_3930_1.JPG)
![Marie Juli 2007 2](/wpexport/2007-07-11-marie-en-maned/img_3932_1.JPG)
![Marie Juli 2007 3](/wpexport/2007-07-11-marie-en-maned/img_3961_1.JPG)


