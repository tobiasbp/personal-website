---
author: admin
date: 2016-01-14 11:38:14+00:00
draft: false
title: Preparing an SD card for Raspberry Pi using the GNU/Linux command line
type: post
url: /2016/01/preparing-an-sd-card-for-raspberry-pi-using-the-gnulinux-command-line/
categories:
- Raspberry Pi
- Tutorial
---

For the purpose of reminding myself, and possible others, here is a quick post describing the steps needed to prepare an SD card with with the NOOBS system installer for Raspberry Pi using the [GNU/Linux](http://www.gnu.org/gnu/why-gnu-linux.html) command line.

I'm using [Ubuntu](http://www.ubuntu.com/), but it should work on any GNU/Linux system.
<!-- more -->


## Download NOOBS


[NOOBS](https://www.raspberrypi.org/downloads/noobs/) is the user friendly installer for the Raspbian GNU/Linux distribution for the Raspberry Pi.

Download a compressed .zip file containing the latest version of NOOBS. At the time of writing, it's version 1.5.0. The URL below, should should be an alias to the latest version at any given time. I'm using _wget_ to download the file.

**wget https://downloads.raspberrypi.org/NOOBS_latest**

Note, that you can also download the .zip file using BitTorrent.

The file I downloaded, is called: _NOOBS_v1_5_0.zip_


## Locate the SD card


Insert the SD card in to your machine.

Having inserted the card. Run the command _dmesg_ to see how the machine responded to the SD card beeing inserted. Here are two lines from the response on my machine.

_....
__[206339.307192] mmc0: new high speed SDHC card at address 0007_
_[206339.307494] mmcblk0: mmc0:0007 SD8GB 7.42 GiB
...._

The device representing my card is _mmcblk0_. Confirm by listing it with the command _ls /dev/mmcblk0_.

If the card allready has partitions on it, they were probably mounted when you inserted it. Let's see if any thing was mounted. Look at the list of mounted devices, and print any lines matching our device.

**_mount | grep mmcblk0_**

_/dev/mmcblk0p5 on /media/tbp/SETTINGS type ext4 (rw,nosuid,nodev,relatime,data=ordered,uhelper=udisks2)_
_/dev/mmcblk0p7 on /media/tbp/root type ext4 (rw,nosuid,nodev,relatime,data=ordered,uhelper=udisks2)_
_/dev/mmcblk0p6 on /media/tbp/boot type vfat (rw,nosuid,nodev,relatime,uid=1000,gid=1000,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,showexec,utf8,flush,errors=remount-ro,uhelper=udisks2)_

Three partitions on my card are mounted. The output is a bit difficult to read, so let's limit the output to just the mount points by only printing the 3rd entry of every line delimited by the space character denoted by ' '.

**_mount | grep mmcblk0 | cut -d ' ' -f 3_**

/media/tbp/SETTINGS
/media/tbp/root
/media/tbp/boot

Let's unmount the partitions.

**_umount /media/tbp/SETTINGS /media/tbp/root /media/tbp/boot_**




## Create a partition on the SD card


We now need to create a single partition on the SD card. We'll do it with _cfdisk_. You will need to run the command with super user privileges by prepending the command with _sudo_ (super user do).

**_sudo cfdisk /dev/mmcblk0_**

If you have any partitions on your card (Like me), you need to delete them. Select a partition by highligting it, and delete it by choosing _[ Delete ]_.

Having deleted all partitions, we need to create a new one. With no partitions left on the card, the _Free space_ should be selected. Select _[ New ]_ to create a new partition. Press enter at each question to accept the defaults of a _Primary_ partition of maximum size.

Select _[ Type ]_ and set the  type of the partition to _W95 FAT32 (LBA)_.

Save the new partition table to the SD card  by selecting _[ Write ]_, and then _Yes._

Exit _cfdisk_ by selecting _[ Quit ]_.


## Create a file system


We need to create a FAT32 filsystem on the partition we created on the card. The partition is presented as a device in _/dev_, like the SD card itself. The name of the device is _mmcblk0p1_. Notice, this is the name of the SD card device with at suffix of _p1_ (Partition 1).

Use _mkfs.vfat_ to create the filesystem. Prepend with _sudo_ to get the needed priviliges.

**_sudo mkfs.vfat /dev/mmcblk0p1_**


## Copy NOOBS to the SD card


Take the SD card out of your machine. Insert it again. It should be mounted automatically. We need to know where the partition is mounted. Let's get the the lines containing the partition's device from the list of mounted filesystems.

**_mount | grep mmcblk0p1 **_| cut -d ' ' -f 3_**_**

_/media/tbp/7BA7-C44B _

The partion is mounted at _/media/tbp/7BA7-C44B_.

I can now unzip the files making up NOOBS and put them on the SD card.

**_unzip NOOBS_v1_5_0.zip -d /media/tbp/7BA7-C44B/_**

_Archive: NOOBS_v1_5_0.zip_
_ inflating: /media/tbp/7BA7-C44B/bcm2708-rpi-b.dtb_
_ inflating: /media/tbp/7BA7-C44B/bcm2708-rpi-b-plus.dtb_
_ inflating: /media/tbp/7BA7-C44B/bcm2709-rpi-2-b.dtb_
_ inflating: /media/tbp/7BA7-C44B/bootcode.bin_
_ inflating: /media/tbp/7BA7-C44B/BUILD-DATA_
_ creating: /media/tbp/7BA7-C44B/defaults/_
_ ........._
_ ........._
_ extracting: /media/tbp/7BA7-C44B/RECOVERY_FILES_DO_NOT_EDIT_
_ inflating: /media/tbp/7BA7-C44B/recovery.img_
_ inflating: /media/tbp/7BA7-C44B/recovery.rfs_
_ inflating: /media/tbp/7BA7-C44B/riscos-boot.bin_

As the last step, let's unmount the partition.

**_umount /media/tbp/7BA7-C44B_**

Eject the SC card, and insert it in to your Raspberry Pi. Booting the machine, should present you with the graphical NOOBS installation menu.
