---
title: "Moving to Hugo From Wordpress"
date: 2020-02-27T23:32:42+01:00
draft: false
---
I'm moving this site from Wordpress to [Hugo](https://gohugo.io/). Hugo is a tool for building static sites (SSGs).
I have the site in `git`, and the site is served by [Netlify](https://www.netlify.com/). Whenever I push my changes in `git`
to [GitLab](https://gitlab.com/tobiasbp/personal-website), Netlify rebuilds the website, and publishes it.
This is MUCH easier than maintaining a Wordpress/MySQL installation. This way of building web sites is known as the
[JAMStack](https://jamstack.org/), as opposed to the classic [LAMP stack](https://en.wikipedia.org/wiki/LAMP_(software_bundle))
consisting of  Linux, Apache, MySQL & PHP.

I used the Python tool [exitwp-for-hugo](https://github.com/wooni005/exitwp-for-hugo) to convert my Wordpress backup (An XML file)
to files apropriate for Hugo. I had to move my images manually. Luckily, I only had a few. Had I had more, it would have
been VERY tedious to do.   

I could get the content from a headless CMS, but for now, I'm fine with writing new posts in separate files
formated in [Markdown](https://www.markdowntutorial.com/) and adding them to my `git` repository.
