---
author: admin
date: 2014-06-05 20:34:40+00:00
draft: false
title: Har opdateret Wordpress
type: post
url: /2014/06/opdateret-wordpress/
categories:
- Life
---

Efter 3 års fravær, har jeg nu opdateret Wordpress til v. 3.9.1 fra v. 2.1.2. Jeg har flyttet sitet til min egen virtuelle maskine hos [DigitalOcean](https://www.digitalocean.com/) som jeg er MEGET tilfreds med.

Jeg forestiller mig, at jeg kan bruge sitet til at gå mere i dybden med diverse nørde-projekter end det er naturligt at gøre på G+.

Oh... And I should probably also start to write in english because of my new geeky focus of this site. The intended audience used to be family and friends.
