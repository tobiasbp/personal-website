---
author: admin
date: 2018-01-05 22:54:10+00:00
draft: false
title: 'Lille Palle: Farmor ska'' ha'' bajer'
type: post
url: /2018/01/lille-palle-farmor-ska-ha-bajer/
categories:
- Musik
---

Jeg har altid troet, at teksten "Farmor ska' ha' bajer" sunget på melodien fra hittet [Vamos a la playa](https://en.wikipedia.org/wiki/Vamos_a_la_playa_(Righeira_song)) fra 1982, var noget nogle børn selv havde fundet på i min ungdom. Nu har jeg opdaget at det er en single med [Lille Palle Andersen](http://lillepalle.dk/). Det anede jeg ikke! Teksten har jeg ikke kunnet finde noget andet sted på nettet. Derfor har jeg skrevet den ind her.

{{< youtube ZFwmB39Y728 >}}

[Mere info på Discogs](https://www.discogs.com/Lille-Palle-Andersen-Farmor-Ska-Ha-Bajer/release/5445265).

> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh Åh\
> \
> Fodboldkamp med stemning\
> Dyrkes uden hæmning\
> Når endelig der blir' scoret\
> Så sir' hun kom til foret!\
> \
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh Åh\
> \
> Og på a'Angleterre\
> Lo en nobel herre\
> Da hun skred ind ad døren\
> Og skreg til inspektøren\
> \
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh Åh\
> \
> [Mellemspil]\
> \
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh Åh\
> \
> Da hun demonstrered'\
> Blev hun arresteret\
> Fra cellen på stationen\
> Der lød et råb fra konen\
> \
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh Åh\
> \
> [Mellemspil]\
> \
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> Kassevis a' bajer\
> Åh åh åh åh åh\
> Der er noget at fejre\
> Åh åh åh åh åh\
> Farmor ska' ha' bajer\
> Åh åh åh åh åh\
> \
> [Fade]
>
