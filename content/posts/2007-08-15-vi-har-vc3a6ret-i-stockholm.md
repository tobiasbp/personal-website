---
author: admin
date: 2007-08-15 11:08:25+00:00
draft: false
title: Vi har været i Stockholm
type: post
url: /2007/08/vi-har-været-i-stockholm/
categories:
- Billeder
- Rejse
---

Vi har været en uge i Stockholm alle tre. Det var en rigtig dejlig tur. Byen er jo helt fantastisk! Jeg har ikke været i Sverige i 10 år. Jeg anede ikke at byen var så flot. Du kan se billeder fra turen som et slideshow nedenfor (kræver flash). Hvis du vil se selve albummet kan du kigge [her](http://picasaweb.google.com/tobiasbp/Stockholm714August2007) hvor billederne er tilgængelige i fuld opløsning.

<embed src="http://picasaweb.google.com/s/c/bin/slideshow.swf" pluginspage="http://www.macromedia.com/go/getflashplayer" height="192" width="288" type="application/x-shockwave-flash" flashvars="host=picasaweb.google.com&RGB=0x000000&feed=http%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2Ftobiasbp%2Falbumid%2F5098140888546727441%3Fkind%3Dphoto%26alt%3Drss"></embed>
