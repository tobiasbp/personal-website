---
author: admin
date: 2018-06-13 20:01:40+00:00
draft: false
title: MP3-afspiller til telefoner med drejeskive
type: post
url: /2018/06/mp3-afspiller-til-telefoner-med-drejeskive/
categories:
- Elektronik
---

Jeg har lavet et apparat der kan kobles til en telefon med drejeskive og simulere at man ringer op til et telefon-nummer. Apparatet er udviklet til [Enigma](http://www.enigma.dk/), det tidligere post- og telegraf-museum.

De numre man kan ringe til, bestemmes ved at man lægger MP3-filer på et microSD card. Apparatet er (i sin nuværende version) baseret på en [Arduino Pro Mini](https://store.arduino.cc/arduino-pro-mini) og MP3-spiller-modulet [DFPlayer Mini](https://www.dfrobot.com/wiki/index.php/DFPlayer_Mini_SKU:DFR0299).

Her er et billede af den første prototype. Printpladen er 5cm x 5xm.

![](/wpexport/2018-06-13-mp3-afspiller-til-telefoner-med-drejeskive/telephone_simulator_01.jpg)


## Brug


Som udgangspunkt, har de numre der kan ringes op til 6 cifre. Hvis de 2 første cifre er 00,  ringes der dog op til numre på 4 cifre (F.eks. 0059). Hvis de 2 første cife er 11, ringes der op til numre på 3 cifre (F.eks. 118).

<!-- more -->
HER SKAL DER VÆRE ET STATE MACHINE-DIAGRAM.


## Lydfiler


Al lyd der afspilles i telefonen kommer fra MP3-filer der ligger på et microSD kort. Kortet skal være formateret som FAT16 eller FAT32, og må ikke være større end 32GB. I roden af kortet, skal  der findes en mappe med navnet _MP3_. MP3-filerne der skal bruges, skal lægges direkte (Ingen undermaper) i denne mappe, og have navne 4 cifre efterfulgt af endelsen _.mp3_.  Eksempler på filnavne, kunne altså være _2231.mp3_ og 000_3.mp3_.

For numre på 6 cifre, er det kun de 4 sidste der bestemmer hvilken MP3-fil der afspilles. Denne begrænsning har den ulempe, at numerene 112222 & 332222 afspiller den samme MP3-fil _2222.mp3_.

For at kunne simulere en fungerende telefon, skal følgende MP3-filer findes på SD-kortet:


* Klartone (0001.mp3): Den lyd der spilles når røret løftes.
* Opringing (0002.mp3): Den lyd der angiver at der ringes op hos modtageren.
* Samtale slut (0003.mp3): Den lyd der spilles når samtalen er færdig. Dvs. når hele den optagelse der er ringet op til er færdig.
* Ingen forbindelse (0004.mp3): Den lyd der spilles hvis der ringes op til et ikke-eksisterende nummer (Et nummer der ikke har en matchende MP3-fil).

Demo-MP3-filer kan hentes på [projektets GitHub-side](https://github.com/tobiasbp/telephone-audio-player/tree/master/audio/MP3).


## Specifikationer


Apparatet drives af 9V jævnstrøm. Afisolerede ledningerne fra strømkilden kan fastgøres ved at stramme skruer på printet. Det er også muligt at forbinde en strømforsyning med et passende han-stik af typen Barrel Jack (5.5mm, pin 2,1mm).  Sleeve skal være - (GND) og center pin skal være + (VCC). Her er en [pdf](http://www.sparkfun.com/datasheets/Prototyping/Barrel-Connector-PJ-202A.pdf) der viser det hunstik der sidder på printet. Her er [en dansk forhandler af en passende strømforsyning](https://elextra.dk/details/H38969/netadapter-100-240v-til-9vdc-6w-660ma-eup).

Printet kan fastgøres via to huller med en diameter på 4mm.


## Øvrigt





* Her er [en forhandler af microSD-kort](https://www.inkpro.dk/produkter/16635-microsd-kort/).
* MP3-filer kan klippes/laves med open source-programmet [Audacity](https://www.audacityteam.org/).
* Apparatet bør kunne drives af et 9V-batteri. Der er uvist hvor lang tid batteriet vil holde.

